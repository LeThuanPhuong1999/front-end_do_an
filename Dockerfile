FROM node:10.21.0

WORKDIR /usr/src/app

# install package
COPY ["package.json","jsconfig.json","preact.config.js","./"]
RUN npm install

COPY . .
RUN npm build
RUN rm -rf src

EXPOSE 5000
CMD [ "npm", "run","serve" ]