import { theme } from '@chakra-ui/core'
import customIcons from './customIcons'

export default {
  ...theme,
  colors: {
    ...theme.colors,
    brand: {
      primary: '#2C5DE5',
      background: '#EBF0FF'
    },
    flat: {
      yellow: '#f9ca24',
      red: '#eb4d4b',
      green: '#6ab04c',
      orange: '#f0932b',
      purple: '#686de0'
    }
  },
  fonts: {
    body: 'Inter, system-ui, sans-serif',
    heading: 'Inter, Georgia, serif',
    mono: 'Inter, Menlo, monospace'
  },
  lineHeights: {
    ...theme.lineHeights,
    H900: '108px',
    H800: '84px',
    H700: '60px',
    H600: '48px',
    H500: '36px',
    H400: '32px',
    H300: '24px',
    H200: '20px',
    H100: '16px',
    subtitle: '32px',
    bodyLarge: '24px',
    bodyMedium: '20px',
    bodySmall: '16px'
  },
  fontSizes: {
    ...theme.fontSizes,
    H900: '72px',
    H800: '56px',
    H700: '40px',
    H600: '32px',
    H500: '24px',
    H400: '20px',
    H300: '16px',
    H200: '14px',
    H100: '12px',
    subtitle: '20px',
    bodyLarge: '16px',
    bodyMedium: '14px',
    bodySmall: '12px'
  },
  space: {
    ...theme.space,
    px: '1px',
    xxs: '4px',
    xs: '8px',
    sm: '12px',
    md: '16px',
    l: '24px',
    x: '32px',
    xl: '64px',
    xxl: '128px'
  },
  size: {
    ...theme.size,
    px: '1px',
    xxs: '4px',
    xs: '8px',
    sm: '12px',
    md: '16px',
    l: '24px',
    x: '32px',
    xl: '64px',
    xxl: '128px'
  },
  icons: {
    ...theme.icons,
    ...customIcons
  }
}
