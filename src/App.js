import { Router } from 'preact-router'
import { hashLinkScroll } from 'utils/router'
import Layout from 'components/layout/base'
import Home from 'containers/home'
import DES from 'containers/des'
import AES from 'containers/aes'
import RSA from 'containers/rsa'
import MD5 from 'containers/MD5'
import Codien from 'containers/codien'

const App = () => {
  return (
    <Layout>
      <Router onChange={hashLinkScroll}>
        <Home path='/' />
        <DES path='/des' />
        {/* <AES path='/aes' /> */}
        <RSA path='/rsa' />
        <MD5 path='/md5' />
        <Codien path='/codien' />
      </Router>
    </Layout>
  )
}

export default App
