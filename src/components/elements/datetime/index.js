import { DatePicker } from 'antd-mobile'
import { Text, Flex } from '@chakra-ui/core'

const CustomChildren = ({ extra, onClick, children }) => {
  return (
    <Flex
      bg='#fff'
      p='xs'
      onClick={onClick}
      alignItems='center'
      justifyContent='space-between'
      border='gray.200'
      borderRadius='4px'
      borderWidth='1px'
    >
      {children && (
        <Text mr='sm' fontWeight='600' fontSize='bodySmall'>
          {children}
        </Text>
      )}
      <Text color='gray.400' fontWeight='600' fontSize='bodySmall'>
        {extra}
      </Text>
    </Flex>
  )
}

export default function DateTime ({ mode = 'date', title, value, onChange }) {
  return (
    <DatePicker
      mode={mode}
      value={value}
      locale={{
        DatePickerLocale: {
          year: '',
          day: '',
          month: ''
        },
        okText: 'Apply',
        dismissText: 'Cancel'
      }}
      title={title}
      onChange={onChange}
    >
      <CustomChildren>{title}</CustomChildren>
    </DatePicker>
  )
}
