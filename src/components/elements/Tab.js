import { forwardRef } from 'preact/compat'
import { Tab } from '@chakra-ui/core'

export default forwardRef((props, ref) => (
  <Tab
    {...props}
    ref={ref}
    _selected={{
      color: 'brand.primary',
      fontWeight: '500',
      borderColor: 'brand.primary'
    }}
    _focus={{ boxShadow: 'none' }}
  />
))
