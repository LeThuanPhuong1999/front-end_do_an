import { Skeleton } from '@chakra-ui/core'

export default () => (
  <div>
    <Skeleton colorStart='#fafbfb' colorEnd='#eee' height='40px' my='20px' />
    <Skeleton colorStart='#fafbfb' colorEnd='#eee' height='20px' my='20px' />
    <Skeleton colorStart='#fafbfb' colorEnd='#eee' height='20px' my='20px' />
  </div>
)
