import { Stack, Button } from '@chakra-ui/core'
import PropTypes from 'prop-types'
import { SegmentedControl } from 'antd-mobile'
import { useTranslation } from 'react-i18next'
import { useState } from 'react'
import dayjs from 'moment'
import DateTime from 'components/elements/datetime'

export default function TimeFilter ({
  onChange,
  currentIndex = 0,
  dateOptions
}) {
  const { t } = useTranslation('home')

  const [isCustomTime, setIsCustomTime] = useState(false)
  const [currentDateIndex, setDateIndex] = useState(currentIndex)

  const [customTimeFrom, setCustomTimeFrom] = useState(
    new Date(
      dayjs()
        .subtract(7, 'day')
        .toISOString()
    )
  )
  const [customTimeTo, setCustomTimeTo] = useState(new Date(dayjs()))

  function handleChange (e) {
    const { selectedSegmentIndex } = e.nativeEvent
    setDateIndex(selectedSegmentIndex)
    if (!dateOptions[selectedSegmentIndex].isCustomTime) {
      onChange({
        index: selectedSegmentIndex,
        date: dateOptions[selectedSegmentIndex]
      })
      setIsCustomTime(false)
    } else {
      setIsCustomTime(true)
    }
  }

  function handleSearchCustom () {
    onChange({
      index: currentIndex,
      date: {
        ...dateOptions[currentDateIndex],
        from: dayjs(new Date(customTimeFrom)).unix(),
        to: dayjs(new Date(customTimeTo)).unix()
      }
    })
  }

  return (
    <>
      <SegmentedControl
        values={dateOptions.map(item => t(item.name))}
        selectedIndex={currentDateIndex}
        onChange={handleChange}
      />

      {isCustomTime && (
        <Stack
          alignItems='center'
          mt='sm'
          mb='-16px'
          justifyContent='space-between'
          isInline
        >
          <DateTime
            mode='date'
            value={customTimeFrom}
            onChange={setCustomTimeFrom}
            title={t('timeRange.custom.from')}
          />
          <DateTime
            mode='date'
            value={customTimeTo}
            onChange={setCustomTimeTo}
            title={t('timeRange.custom.to')}
          />
          <Button size='sm' onClick={handleSearchCustom}>
            {t('timeRange.custom.search')}
          </Button>
        </Stack>
      )}
    </>
  )
}

TimeFilter.propTypes = {
  onChange: PropTypes.func.isRequired,
  currentIndex: PropTypes.number,
  dateOptions: PropTypes.arrayOf(
    PropTypes.shape({
      key: PropTypes.string,
      name: PropTypes.string,
      from: PropTypes.any,
      to: PropTypes.any
    })
  )
}
