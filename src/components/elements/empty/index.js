import { memo } from 'preact/compat'
import { Box, Image, Button, Heading } from '@chakra-ui/core'
import { Link } from 'preact-router'
// import PropTypes from 'prop-types'

function EmptyState ({ label, messenger = 'No data', routerLink }) {
  return (
    <Box
      mx='auto'
      d='flex'
      flexDirection='column'
      justify='center'
      alignItems='center'
      p='xl'
    >
      <Image mx='auto' src='/assets/img/empty.svg' p='md' alt={label} />
      <Heading as='h4' size='H300' lineHeight='H300'>
        {messenger}
      </Heading>
      {routerLink && (
        <Button as={Link} {...routerLink} mt='l' background='brand.background'>
          {label}
        </Button>
      )}
    </Box>
  )
}

export default memo(EmptyState)

// EmptyState.propTypes = {
//   label: PropTypes.string,
//   routerLink: PropTypes.object
// }
