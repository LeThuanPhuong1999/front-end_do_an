import { memo } from 'preact/compat'
import {
  Textarea as VTextarea,
  Text
} from '@chakra-ui/core'



function Textarea({ placeholder = '', label = '', value = '', size = "sm", isDisabled, onValueChange, ...othersProps }) {
  const handleOnChange = (e) => {
    onValueChange(e.target.value)
  }

  return (
    <>
      <Text mb="8px"> {label}</Text>
      <VTextarea
        value={value}
        placeholder={placeholder}
        size={size}
        onChange={handleOnChange}
        isDisabled={isDisabled}
        {...othersProps}
      />
    </>
  );
}

export default memo(Textarea)
