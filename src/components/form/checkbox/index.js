import { memo } from 'preact/compat'
import { FormErrorMessage, FormControl } from '@chakra-ui/core'
import VCheckbox from 'components/elements/checkbox'

const Checkbox = ({ field, form, ...props }) => (
  <FormControl {...props}>
    <VCheckbox
      {...field}
      isChecked={field.value}
      onChange={isChecked => {
        form.setFieldValue(field.name, isChecked)
      }}
    >
      {props.label}
    </VCheckbox>
    <FormErrorMessage>{form.errors.name}</FormErrorMessage>
  </FormControl>
)

export default memo(Checkbox)
