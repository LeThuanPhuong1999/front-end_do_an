import {
  FormControl,
  FormLabel,
  Input,
  FormErrorMessage
} from '@chakra-ui/core'

export default function InputUseHook ({
  getFieldProps,
  touched,
  errors,
  name,
  label,
  placeholder,
  inputType,
  ...props
}) {
  return (
    <FormControl {...props} mb='1' isInvalid={touched[name] && errors[name]}>
      <FormLabel htmlFor={name}>{label}</FormLabel>
      <Input
        {...getFieldProps(name)}
        {...props}
        id={name}
        placeholder={placeholder}
      />
      <FormErrorMessage>{errors[name]}</FormErrorMessage>
    </FormControl>
  )
}
