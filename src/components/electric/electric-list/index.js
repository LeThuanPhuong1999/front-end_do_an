import { Stack, Text } from '@chakra-ui/core'
import PropTypes from 'prop-types'
import ElectricItem from '../electric-item'
import { caculateMoneyPerWh } from 'utils/caculate'
import numeral from 'numeral'
import { normolizeUnit } from 'utils/unit'

export default function ElectricList ({
  items = [],
  unit = 'KWh',
  primaryColor = 'gray.500',
  secondaryColor = 'brand.primary'
}) {
  return (
    <Stack spacing='l'>
      {items.map(item => (
        <Stack spacing='sm' key={item._id}>
          <Text color='gray.500'>{item.name}</Text>
          {item?.data.map(data => (
            <ElectricItem
              key={data._id}
              name={data.name}
              id={data._id}
              primary={`${numeral(data.consume).format(
                '0,0.00'
              )} ${normolizeUnit(unit)}`}
              primaryColor={primaryColor}
              secondary={caculateMoneyPerWh(parseFloat(data.consume))}
              secondaryColor={secondaryColor}
            />
          ))}
        </Stack>
      ))}
    </Stack>
  )
}

ElectricList.PropTypes = {
  unit: PropTypes.string,
  items: PropTypes.shape({
    _id: PropTypes.string,
    name: PropTypes.string,
    data: PropTypes.arrayOf(
      PropTypes.shape({
        _id: PropTypes.string,
        name: PropTypes.string,
        status: PropTypes.string,
        consume: PropTypes.number
      })
    )
  })
}
