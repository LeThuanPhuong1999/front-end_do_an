import { Stack, Text, Box } from '@chakra-ui/core'
import PropTypes from 'prop-types'
import { Link } from 'preact-router'

export default function ElectricItem ({
  id,
  name,
  primary,
  primaryColor = 'gray.500',
  secondary,
  secondaryColor = 'brand',
  status,
  instantIndicator,
  ...props
}) {
  return (
    <Stack
      spacing='xs'
      py='xs'
      justifyContent='space-between'
      isInline
      {...props}
    >
      <Stack alignItems='center' isInline w='60%'>
        {status && (
          <Box
            w='16px'
            h='16px'
            borderRadius='8px'
            bg={status === 'active' ? 'green.400' : 'gray.200'}
          />
        )}
        <Link href={`/devices/${id}`}>
          <Text fontSize='H200' fontWeight='600'>
            {name}
          </Text>
        </Link>
      </Stack>
      <Text
        width='100px'
        fontWeight='600'
        color={primaryColor}
        fontSize='bodySmall'
      >
        {primary}
      </Text>
      {secondary && (
        <Text fontWeight={600} color={secondaryColor} fontSize='bodySmall'>
          {secondary}
        </Text>
      )}
    </Stack>
  )
}

ElectricItem.propTypes = {
  name: PropTypes.string,
  primary: PropTypes.string,
  primaryColor: PropTypes.string,
  secondary: PropTypes.string,
  secondaryColor: PropTypes.string,
  status: PropTypes.string,
  instantIndicator: PropTypes.arrayOf(
    PropTypes.shape({
      _id: PropTypes.string,
      description: PropTypes.string,
      key: PropTypes.string,
      name: PropTypes.string,
      status: PropTypes.number,
      time: PropTypes.any,
      value: PropTypes.number
    })
  )
}
