import { Tabs, TabList, Tab } from '@chakra-ui/core'
import PropTypes from 'prop-types'
import styled from '@emotion/styled'

const TabsWrapper = styled.div`
  display: inline-block;
  overflow: scroll;
  width: calc(100vw - 24px);
  div[role='tablist'] {
    border-bottom: 0px;
    button {
      margin-right: 8px;
    }
    button[aria-selected='true'] {
      color: #2c5de5;
      font-weight: 600;
      border-color: transparent !important;
    }
  }
`

const TabIndicator = ({ telemetries, onChange }) => {
  return (
    <TabsWrapper>
      <Tabs onChange={index => onChange(telemetries[index])} defaultIndex={0}>
        <TabList>
          {telemetries.map(telemetry => (
            <Tab
              backgroundColor='#f1f1f1'
              borderRadius='32px'
              p='xs'
              key={telemetry}
            >
              {telemetry}
            </Tab>
          ))}
        </TabList>
      </Tabs>
    </TabsWrapper>
  )
}

TabIndicator.propTypes = {
  onTabIndicatorChange: PropTypes.func.isRequired
}
export default TabIndicator
