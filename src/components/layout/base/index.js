import { memo } from 'preact/compat'
import { Box, Flex } from '@chakra-ui/core'
import MenuList from './MenuList'

const LayoutBase = ({ children, menuActiveId = 1, title = 'Incidents' }) => {
  const menus = [
    {
      id: 4,
      icon: 'dashboard',
      title: 'Homepage',
      href: '/'
    },
    {
      id: 3,
      icon: 'station',
      title: 'Mã hoá đối xứng',
      href: '/des'
    },
    // {
    //   id: 2,
    //   title: 'aes-128-ecb',
    //   href: '/aes',
    //   icon: 'incidentComponent'
    // },
    {
      id: 5,
      title: 'Mã hoá bất đối xứng',
      href: '/rsa',
      icon: 'incidentComponent'
    },
    {
      id: 6,
      title: 'Mã hoá 1 chiều',
      href: '/md5',
      icon: 'incidentComponent'
    },
    {
      id: 7,
      title: 'Mã hoá cổ điển',
      href: '/codien',
      icon: 'incidentComponent'
    }
  ]
  return (
    <Flex
      pt='60px'
      backgroundColor='white'
      justify="flex-start"
    >
      <Box style={{ marginLeft: '11em' }} mb='xxl' flex={1} ml="xxl">
        {children}
      </Box>
      <Box
        backgroundColor='white'
        borderColor='gray.200'
        position='fixed'
      >
        <MenuList menus={menus} />
      </Box>
    </Flex>
  )
}

export default memo(LayoutBase)
