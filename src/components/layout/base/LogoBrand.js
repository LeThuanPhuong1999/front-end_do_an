import { Text, Image, Stack } from '@chakra-ui/core'
import { useTranslation } from 'react-i18next'
import UserAvatar from './UserAvatar'
import ChangeLanguageComponent from './ChangeLanguageComponent'

export default function LogoBrand () {
  const { t, i18n } = useTranslation('global')

  const changeLanguage = lng => {
    i18n.changeLanguage(lng)
  }

  return (
    <Stack
      isInline
      justifyContent='space-between'
      position='absolute'
      left='0px'
      right='0px'
      bottom='0px'
      borderTopWidth='1px'
      borderBottomColor='gray.200'
      py='md'
      px='md'
    >
      <UserAvatar />
      <ChangeLanguageComponent changeLanguage={changeLanguage} />
      <Stack spacing='xs' isInline alignItems='center'>
        <Image width='30px' src='/assets/img/logo-icon.png' />
        <Text fontSize='H300' lineHeight='H300' fontWeight='bold'>
          {t('name')}
        </Text>
      </Stack>
    </Stack>
  )
}
