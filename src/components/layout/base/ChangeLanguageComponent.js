import { Menu, MenuButton, MenuList, MenuItem, Image } from '@chakra-ui/core'
import { useState } from 'preact/hooks'

const lang = {
  en: {
    icon: 'https://image.flaticon.com/icons/svg/323/323310.svg'
  },
  vi: {
    icon: 'https://image.flaticon.com/icons/svg/323/323319.svg'
  }
}

export default function ChangeLanguageComponent ({ changeLanguage }) {
  const [flatState, setFlatState] = useState('en')

  const handleChangeLanguage = async lang => {
    if (lang === 'vi') {
      setFlatState('vi')
      changeLanguage('vi')
    } else {
      setFlatState('en')
      changeLanguage('en')
    }
  }

  return (
    <Menu>
      <MenuButton
        rounded='full'
        size='30px'
        src={lang[flatState].icon}
        as={Image}
      />
      <MenuList>
        <MenuItem p='sm' onClick={() => handleChangeLanguage('vi')}>
          <Image
            size='2rem'
            rounded='full'
            src={lang.vi.icon}
            alt='Simon the pensive'
            mr='12px'
          />
          <span>Vietnamese</span>
        </MenuItem>
        <MenuItem p='sm' onClick={() => handleChangeLanguage('en')}>
          <Image
            size='2rem'
            rounded='full'
            src={lang.en.icon}
            alt='Simon the pensive'
            mr='12px'
          />
          <span>English</span>
        </MenuItem>
      </MenuList>
    </Menu>
  )
}
