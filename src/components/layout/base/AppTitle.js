import {
  Stack,
  Flex,
  Heading,
  Text,
  Button,
  Icon,
  Menu,
  MenuList,
  MenuButton
} from '@chakra-ui/core'
import { useTranslation } from 'react-i18next'
import DropdownApp from './DropdownApp'

export default function LogoTitle () {
  const { t } = useTranslation('app')
  return (
    <Flex
      alignItems='center'
      flexDirection='row'
      justifyContent='space-between'
    >
      <Stack alignItems='center' spacing='xs' isInline>
        <Flex
          w='32px'
          height='32px'
          borderRadius='8px'
          backgroundColor={t('meta.color')}
          color='white'
          alignItems='center'
          justifyContent='center'
          mr='xs'
        >
          <Icon size='20px' name='appIncident' />
        </Flex>
        <Stack spacing='px'>
          <Heading as='h1' fontSize='H300' lineHeight='H300'>
            {t('meta.name')}
          </Heading>
          <Text fontSize='bodySmall' lineHeight='bodySmall' color='gray.400'>
            {t('meta.description')}
          </Text>
        </Stack>
      </Stack>

      <Menu>
        <MenuButton
          as={Button}
          minW={0}
          p='xxs'
          borderRadius='15px'
          width='30px'
          height='30px'
        >
          <Icon size='16px' name='app' />
        </MenuButton>
        <MenuList closeOnBlur placement='bottom-end'>
          <DropdownApp />
        </MenuList>
      </Menu>
    </Flex>
  )
}

/*

        <Popover
          initialFocusRef={initialFocusRef}
          placement='bottom'
          closeOnBlur={false}
        >
          <PopoverTrigger>
            <Button
              minW={0}
              p='xxs'
              borderRadius='15px'
              width='30px'
              height='30px'
            >
              ::
            </Button>
          </PopoverTrigger>
          <PopoverContent zIndex={4}>
            <PopoverArrow />
            <PopoverCloseButton />
            <DropdownApp />
          </PopoverContent>
        </Popover>
        */
