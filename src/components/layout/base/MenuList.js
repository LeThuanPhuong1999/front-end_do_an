import { memo } from 'preact/compat'
import { Stack } from '@chakra-ui/core'
import Match from 'preact-router/match'
import MenuItem from './MenuItem'

function checkIsActivePath(activePaths = [], currentUrl) {
  return activePaths.some(activePath => currentUrl.indexOf(activePath) >= 0)
}

function checkIsActive(menu, currentUrl) {
  const isActivePaths = menu.activePaths
    ? checkIsActivePath(menu.activePaths, currentUrl)
    : false
  if (menu.href === '/') {
    return isActivePaths || currentUrl === '/'
  }
  return isActivePaths || currentUrl.indexOf(menu.href) >= 0
}

const MenuList = ({ menus }) => {
  return (
    <Match path='/'>
      {({ url }) => (
        <Stack borderRight="1px" borderColor="gray.200" justifyContent='space-around' height="100vh" spacing='sm'>
          {menus.map((menu) => (
            < MenuItem
              // index ==
              paddingRight="8px"
              paddingLeft="8px"
              paddingBottom='5em'
              paddingTop='0'
              isActive={checkIsActive(menu, url)}
              key={menu.id}
              {...menu}
            />
          ))}
        </Stack>
      )}
    </Match>
  )
}


// const MenuList = () => {
//   return (
//     <Stack spacing={8}>
//       <div>fasdfasd</div>
//       <div>fasdfasd</div>
//       <div>fasdfasd</div>
//     </Stack>
//   )
// }
export default memo(MenuList)
