
import { getPhuongApi } from '../config'
import { getFetch, postFetch } from 'utils/fetch'

const baseUrl = getPhuongApi()
export const genKey = () => {
    const apiUrl = baseUrl + '/genKey'
    return getFetch(apiUrl)
}
export const encryptText = (text, key) => {
    const apiUrl = baseUrl + '/encryptText'
    return postFetch(apiUrl, { text, key })
}
export const decode = (text, key) => {
    const apiUrl = baseUrl + '/decode'
    return postFetch(apiUrl, { text, key })
}
export const genKeyDes = () => {
    const apiUrl = baseUrl + '/genKeyDes'
    return getFetch(apiUrl)
}
export const encryptTextDes = (text, key) => {
    const apiUrl = baseUrl + '/encryptTextDes'
    return postFetch(apiUrl, { text, key })
}
export const decodeDes = (text, key) => {
    const apiUrl = baseUrl + '/decodeDes'
    return postFetch(apiUrl, { text, key })
}
export const genKeyAes = (size) => {
    const apiUrl = baseUrl + '/genKeyAes'
    return postFetch(apiUrl, { size })
}
export const encryptTextAes = (text, key) => {
    const apiUrl = baseUrl + '/encryptTextAes'
    return postFetch(apiUrl, { text, key })
}
export const decodeAes = (text, key) => {
    const apiUrl = baseUrl + '/decodeAes'
    return postFetch(apiUrl, { text, key })
}

export const saveMd5 = (text, hased) => {
    const apiUrl = baseUrl + '/save-md5'
    return postFetch(apiUrl, { planText: text, hasedString: hased })
}
export const decodeMd5 = (hasedString) => {
    const apiUrl = baseUrl + '/decode-md5'
    return postFetch(apiUrl, { hasedString })
}
export const encodeCodien = ({ shift, value }) => {
    const apiUrl = baseUrl + '/encode-codien'
    return postFetch(apiUrl, { shift, value })
}

export const decodeCodien = ({ shift, value }) => {
    const apiUrl = baseUrl + '/decode-codien'
    return postFetch(apiUrl, { shift, value })
}