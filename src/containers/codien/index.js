import {
    Box, Button, NumberInput,
    NumberInputField,
    NumberInputStepper,
    NumberIncrementStepper,
    NumberDecrementStepper
} from '@chakra-ui/core'
import Heading from 'components/elements/heading'
import Textarea from 'components/form/textarea'
import { genKey, encryptText, decode, saveMd5, decodeMd5, encodeCodien, decodeCodien } from 'api/phuongApi'
import { useState, useEffect } from 'preact/hooks'
// const Cipher = require('caesar-ciphers').defaultCipher;
// var cipher = new Cipher(3);

const Error = {
    INVALID_KEY: 'INVALID_KEY'
}




const Codien = () => {
    const [message, setMessage] = useState('')
    const [shift, setShift] = useState('')
    const [result, setValue] = useState('')
    const [text, setText] = useState('')
    
    const handleMessageChange = (val) => {
        setMessage(val)
    }
    const handleChangeNumber = (val) => {
        setShift(val)
    }

    const encode = async () => {
        const encoded = await encodeCodien({ shift, value: message })
        setValue(encoded)
    }

    const decode = async () => {
        const decoded = await decodeCodien({ shift, value: result })
        // setValue(decoded)
        setText(decoded)
    }   

    return (
        <>
            <Box p='sm'>
                <Heading color='black' fontSize='H500' lineHeight='H500'>
                    CAESAR
                </Heading>
            </Box>
            <Box px='sm'>
            Trong mật mã học, mật mã Caesar, còn gọi là mật mã dịch chuyển, là một trong những mật mã đơn giản và được biết đến nhiều nhất. Mật mã Caesar là một dạng của mật mã thay thế, trong đó mỗi ký tự trong văn bản được thay thế bằng một ký tự cách nó một đoạn trong bảng chữ cái để tạo thành bản mã.
            Caesar là một dạng mã hóa đại diện cho kiểu mã hóa cổ điển
                <Box py="xl">
                    <Textarea placeholder={"Enter message here"} label={'Nhập PlainText cần mã hóa :'} value={message} onValueChange={handleMessageChange} />
                    Nhập Key(Key là số)
                    <NumberInput onChange={handleChangeNumber} size='sm' width='sm' defaultValue={shift} min ={0} >
                        <NumberInputField />
                    </NumberInput>
                    <Button my="sm" onClick={encode} variantColor="green">Encrypt</Button>
                    <Textarea label={'Sau khi mã hóa CipherText sẽ hiển thị ở đây :'} value={result} onValueChange={() => { }} />
                    <Button my="sm" onClick={decode} variantColor="green">Decrypt</Button>
                    <Textarea label={'Sau khi giải mã sẽ trả về PlainText ban đầu :'} value={text} onValueChange={() => { }} />
                </Box>




            </Box>
        </>
    )
}
export default Codien