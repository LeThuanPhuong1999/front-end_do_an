import { Box } from '@chakra-ui/core'
import Heading from 'components/elements/heading'
import "../style.css"

export default function HomeContainer() {
  return (
    <>
      <Box p='sm'>
        <Heading color='black' fontSize='H500' lineHeight='H500'>
          Homepage
        </Heading>
      </Box>
      <Box px='sm'>
<div className="content">
<div className="item">
    <b className="title">Giới Thiệu Về Các Hệ Mã Hóa</b>
    <p>Trong thời đại số hóa ngày nay, mật mã đóng một vai trò rất quan trọng. Và tôi nghĩ người lập trình viên cần phải trang bị các kiến thức cơ bản về mã hóa. Vì vậy, trong bài viết này tôi muốn giới thiệu đến các bạn tổng quan về các hệ mật mã, cách chúng làm việc, ưu và nhược điểm của từng hệ mật mã, cách chúng phối hợp, bổ sung cho nhau như thế nào.</p>
    <p>Vậy mật mã quan trọng như thế nào, và những ứng dụng của nó trong đời sống cũng như kỹ thuật là gì?</p>
</div>
<div className="item">

<h1 className="text"> Vai trò của mã hóa</h1>
<p>Mã hóa giúp chúng ta che dấu thông tin, đảm bảo tính riêng tư, bí mật của các thông tin nhạy cảm.</p>
<h2> </h2>
<p>Có thể nói mật mã được ứng dụng trong mọi ngóc ngách của công nghệ: lúc bạn lướt facebook, tìm kiếm trên google (riêng cá nhân tôi vẫn thích dùng duckduckgo.com hơn), check mail, giao dịch ngân hàng hay lúc tôi đang gõ những dòng blog này, v.v…</p>

<p>Hơn nữa mật mã còn được sử dụng trong bầu cử, đấu giá ẩn danh, hay mới nổi lên gần đây đó là sự xuất hiện của đồng tiền ảo bitcoin.</p>

<p>Trước khi đi vào tìm hiểu các hệ mã hóa khác nhau, tôi nghĩ cần phải hiểu thế nào là một hệ mã hóa.</p>
</div>
<div className="item">

<h1 className="text" >Định nghĩa hệ mã hóa</h1>
<p>Một hệ mã hóa gồm hai thuật toán, ta ký hiệu <b>E (Encryption – hàm mã hóa)</b> và <b>D (Decryption – hàm giải mã)</b>, và có một điểm cần lưu ý đó là <b>(E, D)</b> phải có thời gian tính đa thức. Ta ký hiệu M là tập các bản rõ (plain text), K là tập các khóa và C là tập các bản mã, hàm<b>(E, D)</b> phải thỏa mãn điều kiện sau:</p>
<div className="container">
  <p>Với m thuộc M, k thuộc K: E(m, k) = c (thuộc C)</p>
  <p> suy ra D(c, k) = D(E(m, k), k) = m</p>
 
</div>
<div className="image1">
  </div>
<p>Thông thường hàm mã hóa E là đa định, tức là với đầu vào (m, k) cố định, sẽ cho ra các đầu ra c khác nhau. Và hàm giải mã D là đơn định, từ đầu vào (c, k) sẽ cho ra một bản rõ duy nhất. Đây cũng là điều hiển nhiên, một hàm giải mã không thể cho ra nhiều đầu ra khác nhau.</p>
<p>Có hai hướng để phân loại các hệ mật mã:</p>
<p>Nếu xét về số bit xử lý, ta có hệ mã hóa dòng và mã hóa khối</p>
<p>Nếu xét theo loại khóa, ta có hệ mã đối xứng (hay còn gọi là khóa bí mật – Private Key) và hệ mã hóa bất đối xứng (hay còn gọi là khóa công khai – Public Key)</p>
</div>
<div className="item">

<h1 className="text">Hệ mã dòng – Stream Cipher</h1>   
<p>Với các hệ mã dòng (stream cipher), ta sẽ xử lý trên từng bit của bản rõ. Một hệ mã dòng rất nổi tiếng đó là One Time Pad (OTP), lưu ý các bạn đừng nhầm lẫn với One Time Password. Với bản rõ m và khóa k có cùng độ dài theo bit, One-Time-Pad được xác định như sau:</p>
<div className="container">
<p>E(m, k) = m XOR k = c</p>   
<p>D(c, k) = c XOR k = (m XOR k) XOR k = m</p>
</div>
<p>Với OTP, khóa k phải đáp ứng 3 điều kiện sau đây:</p>
<p>Độ dài của khóa phải bằng kích thước bản rõ.</p>
<p>Khóa phải được chọn hoàn toàn ngẫu nhiên (truly random)</p>    
<p>Và khóa chỉ được sử dụng một lần</p>
<p>Nếu thỏa mãn 3 điều kiện trên, hệ mã OTP sẽ là an toàn tuyệt đối (perfect security) theo định lý của Clause Shannon, tức là kẻ tấn công sẽ không thể biết được thông tin gì của bản rõ m chỉ từ bản mã c.</p>
<p>Bởi vì các hàm mã-hóa/giải-mã chỉ đơn giản là thực hiện phép toán XOR trên các bit của dữ liệu đầu vào, do đó OTP cho ta tốc độ tính toán rất nhanh.</p>
<p>Do khóa có độ dài bằng bản rõ, nên nếu ta có thể truyền khóa một cách bí mật đến bên nhận thì ta cũng có thể sử dụng cách đó để truyền luôn bản rõ. Đây cũng nhược điểm của OTP. Trong thực tế, người ta thường chọn ngẫu nhiên một khóa k có độ dài nhỏ hơn rất nhiều so với bản rõ, và dùng một hàm tạo số ngẫu nhiên (Pseudo Random Generator – PRG) để mở rộng độ dài của khóa k đó.</p>
<p>Vì vậy, biến thể của OTP được dùng trong thực tế như sau:</p>
<div className="container">
<p>E'(m, k) = E(m, PRG(k)) = m XOR PRG(k) = c</p>
<p>D'(c, k) = D(c, PRG(k)) = (m XOR PRG(k)) XOR PRG(k) = m</p>
</div>
<p>trong đó, k có kích thước nhỏ hơn rất nhiều so với m.</p>
</div>
<div className="item">

<div className="item"></div>
<h1 className="text" >Hệ mã khối – Block Cipher</h1>
<p>Các hệ mã khối, giống như tên gọi, thực hiện tính toán trên từng khối bit có kích thước cố định, thông thường là 64 hoặc 128 bit. Vì vậy khi đầu vào bản rõ có độ dài không là bội số của khối, ta cần phải thực hiện thao tác đệm (padding) sao cho số bit của đầu vào phải là bội số của khối.</p>
<div className="image2">
  </div>
<p>Các hệ mã khối nổi tiếng đó là DES có kích thước khối là 64 (tức là mỗi lần mã hóa một khối bản rõ 64 bits và cho ra khối bản mã 64 bít) và kích thước khóa là 56 bits; AES có kích thước khối là 128 bit, và kích thước khóa là 128, 192 hoặc 256bit.</p>
<p>Thông thường block cipher chậm hơn so với stream cipher, nhưng làm việc tốt với những khối dữ liệu đã biết trước kích thước, ví dụ mã hóa file, mã hóa tin nhắn trên các giao thức như là HTTP …</p>
</div>
<div className="item">

<h1 className="text" >Hệ mã đối xứng – Private Key</h1>
<p>Các hệ mã đối xứng sử dụng chung một khóa K cho cả hai hàm mã hóa và giải mã.</p>
<div className="image3">
  </div>
<p>Các ví dụ về hệ mã dòng (OTP) và mã khối (DES, AES) đều là các hệ mã đối xứng.</p>
<p>Có tốc độ tính toán nhanh, Nhưng lại khó để vận chuyển khóa</p>
</div>
<div>
  <p>Đọc thêm về mã hóa DES tại <a className="link" href="https://vi.wikipedia.org/wiki/DES_(m%C3%A3_h%C3%B3a)">https://vi.wikipedia.org/wiki/DES_(m%C3%A3_h%C3%B3a)</a></p>
</div>
<div className="item">

<h1 className="text">Hệ mã bất đối xứng – Public Key</h1>
<p>Vấn đề đối với các hệ mã đối xứng đó là rất khó để trao chuyển khóa một cách bí mật và các khóa thường gắn liền với một phiên làm việc. Do đó rất khó để quản lý khóa, hơn nữa hệ mã đối xứng không cung cấp cho chúng ta cơ chế chữ ký điện tử.</p>
<div className="image4">
  </div>
<p>Vì vậy, các hệ mã khóa công khai đã ra đời để giải quyết các vấn đề trên, ý tưởng của hệ mã khóa công khai đó là mỗi khóa sẽ gắn liền với một người sử dung (thay vì gắn liền với một phiên làm việc giữa 2 người dùng). Trong hệ mã khóa công khai, khóa sẽ bao gồm một bộ gồm 2 khóa PK (Public Key) và SK (Secret Key). Tin nhắn mã hóa với khóa PK sẽ được giải mã với khóa SK và ngược lại. Khóa PK sẽ được công khai, vì vậy ai cũng có thể sử dụng khóa đấy để mã hóa rồi gửi tin nhắn bí mật cho A, nhưng chỉ có A mới có thể giải mã tin nhắn đó (do chỉ có A mới có khóa SK).</p>
<p>Sơ đồ chung của hệ mã như sau:</p>
<div className="container">

<p>E(m, pk) = c</p>
<p>D(c, sk) = m</p>
</div>
<p>Một hệ mã hóa công khai rất nổi tiếng đó là RSA</p>
<p>So với các hệ mã hóa đối xứng, thì hệ mã hóa bất đối xứng có tốc độ tính toán chậm hơn rất nhiều. Vì vậy, người ta thường dùng hệ mã bất đối xứng để vận chuyển khóa bí mật, sau đó phiên làm việc sẽ được mã hóa trên khóa chung này (sử dụng các hệ mã đối xứng).</p>
</div>
<div>
  <p>Đọc thêm về mã hóa RSA tại <a className="link" href="https://vi.wikipedia.org/wiki/RSA_(m%C3%A3_h%C3%B3a)">https://vi.wikipedia.org/wiki/RSA_(m%C3%A3_h%C3%B3a)</a></p>
</div>
<div className="item">
<h1 className="text">Mã hóa một chiều - hash </h1>
<p>Hàm băm (Hash function) là một hàm mà nó nhận vào một chuỗi có độ dài bất kì, và sinh ra một chuỗi kết quả có độ dài cố định (Gọi là chuỗi hash), dù hai chuỗi dữ liệu đầu vào, được cho qua hàm băm thì cũng sinh ra hai chuỗi hash kết quả khác nhau rất nhiều. Ví dụ như đối với kiểu dữ liệu Hash-table, ta có thể coi đây là một dạng kiểu dữ liệu mảng đặc biệt mà index nó nhận vào là một chuỗi, nó được định nghĩa bằng cách bên trong nó chứa một mảng thông thường, mỗi khi truyền vào index là một chuỗi, thì chuỗi này sẽ đi qua hàm băm và ra một giá trị hash, giá trị này sẽ tương ứng với index thật của phần tử đó trong mảng bên dưới.</p>
<div className="wrapImage5">
<div className="image5">
  </div>
  </div>
<div>
  <p>Đọc thêm về mã hóa một chiều MD5 tại <a className="link" href="https://vi.wikipedia.org/wiki/MD5">https://vi.wikipedia.org/wiki/MD5</a></p>
</div>
</div>
<div className="item">
  <h1 className="text">Mã hóa cổ điển</h1>
<p>Mã hóa cổ điển là phương pháp mã hóa đầu tiên, và cố xưa nhất, và hiện nay rất ít được dùng đến so với các phương pháp khác. Ý tưởng của phương pháp này rất đơn giản, bên A mã hóa thông tin bằng thuật toán mã hóa cổ điển, và bên B giải mã thông tin, dựa vào thuật toán của bên A, mà không dùng đến bất kì key nào. Do đó, độ an toàn của thuật toán sẽ chỉ dựa vào độ bí mật của thuật toán, vì chỉ cần ta biết được thuật toán mã hóa, ta sẽ có thể giải mã được thông tin.</p>
<div className="wrapImage6">
<div className="image6">
  </div>
  </div>
<div>
  <p>Đọc thêm về mã hóa cổ điển Caesar tại <a className="link" href="https://vi.wikipedia.org/wiki/M%E1%BA%ADt_m%C3%A3_Caesar">https://vi.wikipedia.org/wiki/M%E1%BA%ADt_m%C3%A3_Caesar</a></p>
</div>
</div>
<div className="right">
<div className="item">
  <p>Sinh viên thực hiện :                     </p>
  </div>
  <div className="item">
<p>Lê Thuận Phương - MSSV: 17520923</p>
<p>Trương Thảo Vy- MSSV: 17521281</p>
<p>Huỳnh Khoa - MSSV: 17520640</p>
</div>
</div>
</div>

    </Box>
    </>
  )
}
