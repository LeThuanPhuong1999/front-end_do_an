import dayjs from 'moment'

const DATES = {
  YESTERDAY: 'YESTERDAY',
  DAY15: 'DAY15',
  DAY30: 'DAY30',
  CUSTOM: 'CUSTOM'
}

export default [
  {
    key: DATES.YESTERDAY,
    name: 'timeRange.yesterday',
    to: dayjs(new Date())
      .subtract(1, 'day')
      .endOf('day')
      .unix(),
    from: dayjs(new Date())
      .subtract(1, 'day')
      .startOf('day')
      .unix()
  },
  {
    key: DATES.DAY15,
    name: 'timeRange.day15',
    to: dayjs(new Date()).unix(),
    from: dayjs(new Date())
      .subtract(15, 'day')
      .unix()
  },
  {
    key: DATES.DAY30,
    name: 'timeRange.day30',
    to: dayjs(new Date()).unix(),
    from: dayjs(new Date())
      .subtract(30, 'day')
      .unix()
  },
  {
    isCustomTime: true,
    key: DATES.CUSTOM,
    name: 'timeRange.custom.name',
    to: '',
    from: ''
  }
]
