import { Box, Button } from '@chakra-ui/core'
import Heading from 'components/elements/heading'
import Textarea from 'components/form/textarea'
import { genKey, encryptText, decode } from 'api/phuongApi'
import { useState } from 'preact/hooks'

const Error = {
    INVALID_KEY: 'INVALID_KEY'
}




const Rsa = () => {
    const [privateKey, setPrivateKey] = useState('')
    const [privateKey4Decode, setPrivateKey4Decode] = useState('')
    const [publicKey, setPublicKey] = useState('')
    const [privateKeyGen, setPrivateKeyGen] = useState('')
    const [publicKeyGen, setPublicKeyGen] = useState('')
    const [planText, setPlanText] = useState('')
    const [encryptedText, setEncryptedText] = useState('')
    const [encryptedText4Decode, setEncryptedText4Decode] = useState('')
    const [output, setOutput] = useState('')

    const generateKey = async () => {
        let result = await genKey()
        setPublicKeyGen(result.data.publicKey)
        setPrivateKeyGen(result.data.privateKey)
    }
    const handleEncrypt = async () => {
        const result = await encryptText(planText, publicKeyGen) // publicKey || publicKeyGen
        if (result.error == true) alert(Error.INVALID_KEY)
        setEncryptedText(result.data)
    }
    const handleDecode = async () => {
        const result = await decode(encryptedText, privateKeyGen) // privateKey4Decode || privateKeyGen  ===> encryptedText4Decode || encryptedText
        if (result.data == null) alert(Error.INVALID_KEY)
        setOutput(result.data)
    }
    const handleOnchangePlanText = (text) => {
        setPlanText(text)
    }
    const handleOnPublicKeyChange = (key) => {
        setPublicKey(key)
    }
    const handleChangePrivateKey = (key) => {
        setPrivateKey(key)
    }
    const handleEncryptdTextChange = (text) => {
        setEncryptedText4Decode(text)
    }
    const handleChangePrivateKey4Decode = key => {
        setPrivateKey4Decode(key)
    }
    return (
        <>
            <Box p='sm'>
                <Heading color='black' fontSize='H500' lineHeight='H500'>
                    RSA
                </Heading>
            </Box>
            <Box px='sm'>
            Thuật toán được Ron Rivest, Adi Shamir và Len Adleman mô tả lần đầu tiên vào năm 1977 tại Học viện Công nghệ Massachusetts (MIT). Tên của thuật toán lấy từ 3 chữ cái đầu của tên 3 tác giả.
            RSA là một dạng mã hóa đại diện cho kiểu mã hóa bất đối xứng
            
            
                <Box py="sm">
                    <Button my="sm" onClick={generateKey} variantColor="green">Generate key</Button>
                    <div>Click vào generateKey sẽ tạo ra publicKey và privateKey ngẫu nhiên</div>
                    <Textarea placeholder={"Public Key will appear here"} label={'Public Key'} value={publicKeyGen} />
                    <Textarea placeholder={"Private Key will appear here"} label={'Private Key'} value={privateKeyGen} onValueChange={handleChangePrivateKey} />
                </Box>


                <Box py="sm">
                    <Textarea placeholder={"Nhập PlainText"} label={'Nhập PlainText cần mã hóa :'} value={planText} onValueChange={handleOnchangePlanText} />
                    {/* <Textarea placeholder={"Enter Public key"} label={'Enter Public key'} value={publicKey} onValueChange={handleOnPublicKeyChange} /> */}
                    <Button my="sm" onClick={handleEncrypt} variantColor="green">Encrypt</Button>
                    <Textarea isDisabled placeholder={"Encrypted Output"} label={'Sau khi mã hóa CipherText sẽ hiển thị ở đây :'} value={encryptedText} />
                </Box>


                {/* <Textarea marginBottom="sm" placeholder={"Nhập chuỗi mã hoá (Base 64)"} label={'Nhập chuỗi mã hoá '} onValueChange={handleEncryptdTextChange} value={encryptedText4Decode} /> */}
                {/* <Textarea marginBottom="sm" placeholder={"Nhập Private key"} label={'Nhập Private key'} onValueChange={handleChangePrivateKey4Decode} value={privateKey4Decode} /> */}
                <Button my="sm" variantColor="green" onClick={handleDecode}>Decrypt</Button>
                <Textarea isDisabled placeholder={"Chuỗi được giải mã"} label={'Sau khi giải mã sẽ trả về PlainText ban đầu :'} value={output} />

            </Box>
        </>
    )
}
export default Rsa