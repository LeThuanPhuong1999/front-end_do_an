import { Box, Button } from '@chakra-ui/core'
import Heading from 'components/elements/heading'
import Textarea from 'components/form/textarea'
import { genKey, encryptText, decode, saveMd5, decodeMd5 } from 'api/phuongApi'
import { useState, useEffect } from 'preact/hooks'
import md5 from 'md5'

const Error = {
    INVALID_KEY: 'INVALID_KEY'
}




const MD5 = () => {
    const [planText, setPlanText] = useState('')
    const [md5String, setMd5String] = useState('')
    const [isHashFound, setIsHashFound] = useState(false)
    const [foundedText, setFoundedText] = useState(false)
    useEffect(async () => {
        const decodedMd5 = await decodeMd5(md5String)
        if (decodedMd5 === null) {
            return setIsHashFound(false)
        }
        setFoundedText(decodedMd5.planText)
        setIsHashFound(true)
    }, [md5String]);

    const hashMD5 = () => {
        const hashedString = md5(planText)
        setMd5String(hashedString)
        saveMd5(planText, hashedString)
    }
    // const DecodeMd5 = async () => {
    //     return decodeMd5(md5String)
    //     // if (decoded === null) {
    //     //     return alert('Sorry, this hash is not in our database')
    //     // }
    //     // setPlanText(decoded.planText)
    // }
    const handlePlanTextChange = (val) => {
        setPlanText(val)
    }
    const handleMd5Change = async (val) => {
        setMd5String(val)
    }
    const Description = ({ isHashFound, foundedText }) => {
        return (
            <div>
                {
                    isHashFound && (
                        <span>Plantext có thể là: <b>{foundedText}</b></span>
                    )
                }
                {
                    !isHashFound && (
                        <span style={{ color: 'red' }}>Không tìm thấy PlainText trong Database </span>
                    )
                }
            </div>
        )


    }

    const _isShowDescription = () => {
        console.log(md5String, '------md5String')
        return md5String !== ''
    }

    return (
        <>
            <Box p='sm'>
                <Heading color='black' fontSize='H500' lineHeight='H500'>
                    MD5
                </Heading>
            </Box>
            <Box px='sm'>
            MD5 (viết tắt của tiếng Anh Message-Digest algorithm 5) là một hàm băm mật mã học được sử dụng phổ biến với giá trị Hash dài 128-bit. Là một chuẩn Internet (RFC 1321), MD5 đã được dùng trong nhiều ứng dụng bảo mật, và cũng được dùng phổ biến để kiểm tra tính toàn vẹn của tập tin. Một bảng băm MD5 thường được diễn tả bằng một số hệ thập lục phân 32 ký tự.
            Hash MD5 là một dạng mã hóa đại diện cho kiểu mã hóa một chiều
                <Box py="xl">
                    <Button my="sm" onClick={hashMD5} variantColor="green">Encrypt</Button>
                    {/* <Button my="sm" onClick={DecodeMd5} variantColor="green">Decrypt</Button> */}
                    <Textarea placeholder={"Enter PlainText"} label={'Nhập PlainText cần mã hóa :'} value={planText} onValueChange={handlePlanTextChange} />
                    <Textarea placeholder={"MD5 hash will appear here"} label={'PlainText sau khi hash sẽ hiển thị ở đây :'} value={md5String} onValueChange={handleMd5Change} />

                    {
                        _isShowDescription() && (
                            <Description isHashFound={isHashFound} foundedText={foundedText} />
                        )
                    }
                </Box>




            </Box>
        </>
    )
}
export default MD5