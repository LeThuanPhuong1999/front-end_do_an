import { Box, Button, Flex } from '@chakra-ui/core'
import Heading from 'components/elements/heading'
import Textarea from 'components/form/textarea'
import { encryptTextDes, decodeDes, genKeyDes } from 'api/phuongApi'
import { useState } from 'preact/hooks'

const Error = {
    INVALID_KEY: 'INVALID_KEY'
}




const Des = () => {
    const [privateKey4Decode, setPrivateKey4Decode] = useState('')
    const [publicKey, setPublicKey] = useState('')
    const [publicKeyGen, setPublicKeyGen] = useState('')
    const [planText, setPlanText] = useState('')
    const [encryptedText, setEncryptedText] = useState('')
    const [encryptedText4Decode, setEncryptedText4Decode] = useState('')
    const [output, setOutput] = useState('')

    const generateKey = async () => {
        let result = await genKeyDes()
        setPublicKeyGen(result.data)
    }
    const handleEncrypt = async () => {
        const result = await encryptTextDes(planText, publicKeyGen) // publicKey || publicKeyGen
        if (result.error == true) alert(Error.INVALID_KEY)
        setEncryptedText(result.data)
    }
    const handleDecode = async () => {
        const result = await decodeDes(encryptedText, publicKeyGen)//privateKey4Decode || publicKeyGen   =====>  encryptedText4Decode || encryptedText
        if (result.data == null) alert(Error.INVALID_KEY)
        setOutput(result.data)
        // setPlanText(result.data)
    }
    const handleOnchangePlanText = (text) => {
        setPlanText(text)
    }
    const handleOnPublicKeyChange = (key) => {
        setPublicKey(key)
    }
    const handleEncryptdTextChange = (text) => {
        setEncryptedText4Decode(text)
    }
    const handleChangePrivateKey4Decode = key => {
        setPrivateKey4Decode(key)
    }
    return (
        <>
            <Box p='sm'>
                <Heading color='black' fontSize='H500' lineHeight='H500'>
                    DES
                </Heading>
            </Box>
            <Flex direction="column" justify="space-around" >
            DES (viết tắt của Data Encryption Standard), là một dạng mã hóa đại diện cho kiểu mã hóa đối xứng
                <Box mb="l">
                    <Textarea isDisabled placeholder={"Key will appear here"} label={'Key(Click vào generateKey sẽ tạo ra Key ngẫu nhiên )'} value={publicKeyGen} bg="blue" />
                    <Button mt="sm" onClick={generateKey} variantColor="green" >Generate key</Button>
                </Box>

                <Box my="l">
                    <Textarea placeholder={"Nhập PlainText"} label={'Nhập PlainText cần mã hóa : '} value={planText} onValueChange={handleOnchangePlanText} />
                    {/* <Textarea placeholder={"Nhập key"} label={'Nhập key'} value={publicKey} onValueChange={handleOnPublicKeyChange} /> */}
                    <Button my="sm" onClick={handleEncrypt} variantColor="green">Encrypt</Button>
                    <Textarea isDisabled placeholder={"Encrypted Output"} label={'Sau khi mã hóa CipherText sẽ hiển thị ở đây : '} value={encryptedText} />
                </Box>

                <Box mt="l">
                    <Button style={{ marginLeft: '1em' }} variantColor="green" onClick={handleDecode}>Decrypt</Button>
                    {/* <Textarea placeholder={"Nhập chuỗi mã hoá (Base 64)"} label={'Nhập chuỗi mã hoá '} onValueChange={handleEncryptdTextChange} value={encryptedText4Decode} /> */}
                    {/* <Textarea placeholder={"Nhập key"} label={'Nhập key'} onValueChange={handleChangePrivateKey4Decode} value={privateKey4Decode} /> */}
                    <Textarea isDisabled placeholder={"Chuỗi được giải mã"} label={'Sau khi giải mã sẽ trả về PlainText ban đầu :'} value={output} />
                </Box>

            </Flex>
        </>
    )
}
export default Des