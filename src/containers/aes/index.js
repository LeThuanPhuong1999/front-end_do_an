import { Box, Button, Flex } from '@chakra-ui/core'
import Heading from 'components/elements/heading'
import Textarea from 'components/form/textarea'
import { genKeyAes, encryptTextAes, decodeAes } from 'api/phuongApi'
import { useState } from 'preact/hooks'

const Error = {
    INVALID_KEY: 'INVALID_KEY'
}




const Aes = () => {
    const [privateKey4Decode, setPrivateKey4Decode] = useState('')
    const [publicKey, setPublicKey] = useState('')
    const [publicKeyGen, setPublicKeyGen] = useState('')
    const [planText, setPlanText] = useState('')
    const [encryptedText, setEncryptedText] = useState('')
    const [encryptedText4Decode, setEncryptedText4Decode] = useState('')
    const [output, setOutput] = useState('')
    const [size, setSize] = useState('128');
    const generateKey = async () => {
        let result = await genKeyAes(size)
        setPublicKeyGen(result.data)
    }
    const handleEncrypt = async () => {
        const result = await encryptTextAes(planText, publicKey)
        if (result.error == true) alert(Error.INVALID_KEY)
        setEncryptedText(result.data)
    }
    const handleDecode = async () => {
        const result = await decodeAes(encryptedText4Decode, privateKey4Decode)
        if (result.data == null) alert(Error.INVALID_KEY)
        setOutput(result.data)
    }
    const handleOnchangePlanText = (text) => {
        setPlanText(text)
    }
    const handleOnPublicKeyChange = (key) => {
        setPublicKey(key)
    }
    const handleChangePrivateKey = (key) => {
        setPrivateKey(key)
    }
    const handleEncryptdTextChange = (text) => {
        setEncryptedText4Decode(text)
    }
    const handleChangePrivateKey4Decode = key => {
        setPrivateKey4Decode(key)
    }
    return (
        <>
            <Box p='sm'>
                <Heading color='black' fontSize='H500' lineHeight='H500'>
                    AES
                </Heading>
            </Box>
            <Flex px='sm' direction="column" justify="space-aground">
                <Box mb="l">
                    <div>
                        <Button onClick={generateKey} variantColor="green">Generate key</Button>
                        <span> Size </span>
                        <select value={size} onChange={(e) => setSize(e.target.value)}>
                            <option value="128">128</option>
                            <option value="192">192</option>
                            <option value="256">256</option>
                        </select>
                        <span> bits </span>
                    </div>
                    <Textarea isDisabled placeholder={"Key will appear here"} label={'Key'} value={publicKeyGen} />
                </Box>

                <Box my="l">
                    <Textarea placeholder={"Nhập plantext"} label={'Nhập plantext'} value={planText} onValueChange={handleOnchangePlanText} />
                    <Textarea placeholder={"Nhập key"} label={'Nhập key ( Key must be 128 bits (16 bytes), 192 bits (24 bytes) or 256 bits (32 bytes) long):'} value={publicKey} onValueChange={handleOnPublicKeyChange} />
                    <Button my="sm" onClick={handleEncrypt} variantColor="green">Encrypt</Button>
                    <Textarea isDisabled placeholder={"Encrypted Output"} label={'Chuỗi được giải mã (Base 64)'} value={encryptedText} />
                </Box>

                <Box mt="l">
                    <Textarea placeholder={"Nhập chuỗi mã hoá (Base 64)"} label={'Nhập chuỗi mã hoá '} onValueChange={handleEncryptdTextChange} value={encryptedText4Decode} />
                    <Textarea placeholder={"Nhập key"} label={'Nhập key'} onValueChange={handleChangePrivateKey4Decode} value={privateKey4Decode} />
                    <Textarea isDisabled placeholder={"Chuỗi được giải mã"} label={'Chuỗi được giải mã:'} value={output} />
                    <Button variantColor="green" onClick={handleDecode}>Decrypt</Button>
                </Box>

            </Flex>
        </>
    )
}
export default Aes