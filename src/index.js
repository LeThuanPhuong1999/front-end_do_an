import 'preact/debug'
import 'preact/devtools'
import { ThemeProvider, CSSReset } from '@chakra-ui/core'
import theme from 'constants/theme'
import App from './App'

const Index = () => {
  return (
    <ThemeProvider theme={theme}>
      <CSSReset />
      <App />
    </ThemeProvider>
  )
}

export default Index
