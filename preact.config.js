const styled = require('preact-cli-plugin-styled-components')

export default (config, env, helpers) => {
  styled(config, env, helpers)
  const { rule } = helpers.getLoadersByName(config, 'babel-loader')[0]
  const babelConfig = rule.options
  babelConfig.plugins.push([
    require.resolve('@babel/plugin-proposal-optional-chaining')
  ])

  babelConfig.plugins.push([
    require.resolve('@babel/plugin-proposal-nullish-coalescing-operator')
  ])

  if (process.env.NODE_ENV === 'production') {
    babelConfig.plugins.push([
      require.resolve('babel-plugin-transform-remove-console')
    ])
  }

  // babelConfig.plugins.push([
  //   require.resolve('@babel/plugin-transform-react-jsx-source')
  // ])

  babelConfig.plugins.push([
    require.resolve('babel-plugin-module-resolver'),
    {
      alias: {
        components: './src/components',
        containers: './src/containers',
        graph: './src/graph',
        constants: './src/constants',
        routes: './src/routes',
        utils: './src/utils',
        context: './src/context',
        hoc: './src/hoc',
        api: './src/api',
        config: './src/config'
      }
    }
  ])

  babelConfig.plugins.push([
    require.resolve('babel-plugin-import'),
    { libraryName: 'antd-mobile', style: 'css' }
  ])
}
